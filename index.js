// use direcdtive keyword require to make a
// module useful in the current file/module
const express = require('express')

// express function is our server
const app = express()

// use require mongoose module to be used
// in our entry point file index.js
const mongoose = require('mongoose')

// connect to mongoDB Atlas
const port = 6969

// MIDDLEWARES
// express.json is an express framework to parse incoming JSON payloads
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// connecting userRoutes module to index.js entry point
const userRoutes = require('./routes/userRoutes')

// connect to mongoDB databse
mongoose.connect(
  'mongodb+srv://admin:admin1234@zuitt-bootcamp.wvvcs.mongodb.net/courseBooking?retryWrites=true&w=majority',
  { useNewUrlParser: true, useUnifiedTopology: true }
)

// notification
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error! 💀'))
db.once('open', () => {
  console.log(`Connected to Database 💪😎💯`)
})
db.once('open', () => {
  console.log(`Connected to Database 💪😎🤳💯`)
})

// middleware entry point url (root url before any endpoints)
app.use('/api/users', userRoutes)

// server listenign to port 6969
app.listen(port, () => console.log(`Server is running at ${port} 💯`))
