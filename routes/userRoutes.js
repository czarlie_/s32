// create routes
const express = require('express')

// import to use .Router() method
// because .Router() is an express method
// Router() handles the requests
const router = express.Router()

// syntax: router.method('uri', <request listener>)
router.get('/', (req, res) => {
  console.log(`Hello from userRoutes 👋`)
  res.send(`Hello from userRoutes 👋🔥`)
})
router.post('/', (req, res) => {
  console.log(`Hello from postRequest 👋`)
  res.send(`Hello ${req.body.name} 👋🔥`)
})

module.exports = router

// nakaw index ***
// routes
// http://localhost:6969/users
// app.get('/users', (req, res) => {
//   console.log(`hello 🤡`)
//   res.send(`hello 🤡`)
// })

// app.post('/users', (req, res) => {
//   let name = req.body.name
//   res.send(`Hello ${name} 👋`)
// })
